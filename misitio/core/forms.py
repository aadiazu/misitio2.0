from django import forms 
from django.forms import ModelForm
from .models import Contacto



class ContactoForm(ModelForm):
    class Meta:
        model = Contacto
        fields = ['name', 'lastName', 'birthDate', 'email', 'imagen', 'content']
        # widgets = {
        #     'name': forms.TextInput(attrs={'class': 'form-control'}),
        #     'lastName': forms.TextInput(attrs={'class': 'form-control'}),
        #     'birthDate': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
        #     'email': forms.TextInput(attrs={'class': 'form-control'}),
        #     'imagen': forms.FileInput(attrs={'class': 'form-control', 'type': 'file'}),
        #     'content': forms.Textarea(attrs={'class': 'form-control', 'cols':200, 'rows':3})
        # }