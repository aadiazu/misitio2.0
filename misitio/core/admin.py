from django.contrib import admin
from .models import Contacto

# Register your models here.

class ContactoAdmin(admin.ModelAdmin):
    list_display = ['name', 'lastName', 'birthDate', 'email', 'content']
    search_fields = ['name']
    list_filter = ['birthDate']
    list_per_page = 5

admin.site.register(Contacto, ContactoAdmin)