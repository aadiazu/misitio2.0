const activeMenu = () => {
  const openMenuButton = document.getElementById("open-menu-button");
  const closeMenuButton = document.getElementById("close-menu-button");
  const mainMenu = document.getElementById("main-menu");
  if (openMenuButton && mainMenu && closeMenuButton) {
    openMenuButton.addEventListener('click', () => {
      mainMenu.classList.add('active');
    });
    closeMenuButton.addEventListener('click', () => {
      mainMenu.classList.remove('active');
    });
  }
};

activeMenu();

function restablecer(){
  alert("Se ha restablecido el texto");
}

function guardar(){
	alert("Los datos han sido enviados correctamente");
  }
  
  function evento_onfocus(x) {
    x.style.background = "#ECF0F1";
  }
