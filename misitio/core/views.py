from django.shortcuts import render
from .forms import ContactoForm
from .models import Contacto
from django.contrib import messages
from django.shortcuts import redirect, get_object_or_404

from rest_framework import viewsets
from .serializers import ContactoSerializer

# Create your views here.

def home(request):
    return render(request, "core/home.html")

def contact(request):
    form = ContactoForm()

    data = {'form': form}

    if request.method == 'POST':
        formulario = ContactoForm(request.POST, request.FILES) 
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Datos guardados correctamente"
 
    return render(request, "core/contact.html", data)

def aboutUs(request):
    return render(request, "core/about-us.html")

def galery(request):

    lista = Contacto.objects.all()
    return render(request, "core/galeria.html", {'lista': lista})


def projects(request):
    return render(request, "core/projects.html")

def listado(request):
    user = request.user

    if user.has_perm('core.alumno'):
    
        lista = Contacto.objects.all()
        return render(request, "core/listado.html", {
            'lista': lista
        })
    else:
        return render(request, "core/listado.html")

def eliminar_imagen(request, id):
    lista = Contacto.objects.get(id=id)

    try:
        lista.delete()
        mensaje = "Eliminado correctamente"
        messages.success(request, mensaje)
    except:
        mensaje = "No se ha podido eliminar"
        messages.error(request, mensaje)

    return redirect('listado')

def modificar_contacto(request, id):
    lista = Contacto.objects.get(id=id)

    data = {
        'form': ContactoForm(instance=lista)
    }

    if request.method == 'POST':
            formulario = ContactoForm(request.POST, request.FILES, instance=lista) 
            if formulario.is_valid():
                formulario.save()
                return redirect(to='listado')
    return render(request, "core/modificar.html", data)

class ContactoViewset(viewsets.ModelViewSet):
    queryset = Contacto.objects.all()
    serializer_class = ContactoSerializer


# def ingreso_usuario(request):
#         return render(request, "core/login.html")