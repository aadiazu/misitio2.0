from django.db import models
from datetime import date
from django.utils.translation import ugettext as _
from django.dispatch import receiver
from allauth.account.signals import user_signed_up

# Create your models here.

class Contacto(models.Model):
    name = models.CharField('Nombre', max_length=100) 
    lastName = models.CharField('Apellido', max_length=100)
    birthDate = models.DateField('Fecha solicitud')
    email = models.EmailField('E-mail')
    content = models.TextField('Descripción', blank=True)
    imagen = models.ImageField('Imagen de tu Proyecto', null=True)


    def __str__(self):
        return  self.name 

    class Meta:
        permissions = (
            ('profesor',_('Es profesor')),
            ('alumno',_('Es alumno')),
        )

def create_user_profile(request, user, **kwargs):
    profile = Profile.objects.create(user=user)
    profile.save()