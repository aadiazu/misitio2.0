"""misitio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from core import views
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy
from django.contrib.auth.forms import AdminPasswordChangeForm

from rest_framework import routers


router = routers.DefaultRouter()
router.register('contacto',views.ContactoViewset)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name="home"),
    path('contact/', views.contact, name="contact"),
    path('about-us/', views.aboutUs, name="about-us"),
    path('galery/', views.galery, name="galeria"),
    path('projects/', views.projects, name="projects"),
    path('listado/', views.listado, name="listado"),
    path('modificar/<id>/', views.modificar_contacto, name="modificar_contacto"),
    path('eliminar/<id>/', views.eliminar_imagen, name="eliminar_imagen"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('allauth.urls')),
    path('api/', include(router.urls)),
    
    url(
        r'^password/recovery/$',
        auth_views.PasswordResetView.as_view(
            template_name='auth/password_reset_form.html',
            html_email_template_name='auth/password_reset_email.html',
        ),
        name='password_reset',
    ),

    url(
        r'^password/recovery/done/$',
        auth_views.PasswordResetDoneView.as_view(
            template_name='auth/password_reset_done.html',
        ),
        name='password_reset_done',
    ),

    url(
        r'^password/recovery/(?P<uidb64>[0-9A-Za-z_\-]+)/'
        r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(
            success_url=reverse_lazy('home'),
            post_reset_login=True,
            template_name='auth/password_reset_confirm.html',
            post_reset_login_backend=(
                'django.contrib.auth.backends.AllowAllUsersModelBackend'
            ),
        ),
        name='password_reset_confirm',
    ),

    # url(
    #     r'^password_update/$',
    #     auth_views.password_change,
    #     {
    #         'template_name': 'auth/password_change_form.html',
    #         'post_change_redirect': 'auth_password_change_done',
    #     },
    #     name='auth_password_change',
    # ),

    # url(
    #     r'^password_update_done/$',
    #     auth_views.password_change_done,
    #     {
    #         'template_name': 'auth/password_change_done.html',
    #     },
    #     name='auth_password_change_done',
    # ),
   
       
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)